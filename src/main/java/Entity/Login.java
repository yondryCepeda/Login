package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.servlet.http.HttpServlet;

@Entity
@Table(name = "Login")
@NamedQueries({
	@NamedQuery(name = "Login.ListaUsuarios", query = "SELECT a FROM Login a WHERE a.nombre = :user AND a.contraseña = :password")
	
})

@ManagedBean
@RequestScoped
public class Login extends HttpServlet implements Serializable{
	
	private static final long serialVersionUID = -6617147787935984119L;
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
	EntityManager em = emf.createEntityManager();
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "CONTRASEÑA")
	private String contraseña;

	private List<Login> lista = new ArrayList<>();
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	@Override
	public String toString() {
		return "Login [id=" + id + ", nombre=" + nombre + ", contraseña=" + contraseña + "]";
	}
	public String verificar(String user, String password){
	 lista = em.createNamedQuery("Login.ListaUsuarios", Login.class)
			.setParameter("user", user)
			.setParameter("password", password)
			.getResultList();
	 
	 	return "Bienvenido";
	}
	
	
/*	
	public String verificar(String usuario, String password){
		
		
			
		System.out.println("Lista :" + listaUsuarios);
		
			System.out.println("Error");
			return "Bienvenido";
		}*/
}
